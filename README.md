# Syntx (parsing framework)
## Introduction
Syntx is a parsing framework that makes it possible to describe a grammar in an
EBNF-like manner in C++. By simply composing a declarative set of rules a
parser can be generated that produces an abstract syntax tree (AST) if a text
is processed successfully. In case of a syntax error, it returns as much information
as it is possible to extract and creates a nice, informative error message.

As an example, the EBNF
([https://www.w3.org/TR/REC-xml/#sec-notation](https://www.w3.org/TR/REC-xml/#sec-notation))
of JSON ([http://www.json.org](http://www.json.org/))
is given below (this is one possible implementation of the grammar).  Higher
level rules are written in lower-case, low-level rules (terminal symbols) are
written in upper-case.

```
json = value
value = NUMBER | STRING | "null" | "true" | "false" | object | array;
object = "{" (key_value ("," key_value)* )? "}"
key_value = STRING ":" value
array = "[" (value ("," value)* )? "]"
```

The following listing gives the same set of rule using the Syntx framework:

```cpp
using namespace h119::syntx::parser;

rule json, value, object, key_value, array;

json = value << -epsilon{};
value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
key_value = -string{} << -character{":"} << -value;
array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
```
Please note the similarities and slight differences between the two listings.

First of all, in EBNF concatenation, the sequence of rules is expressed by
simply writing the elements after eachother. In C++ an operator is needed for
this purpose -- this is the ``<<`` operator (an operator already used for
similar purposes with C++ streams).

The ``*`` operator that matches zero or more occurences is the same in the EBNF
and Syntx -- the only difference is that the one-operand ``*`` operator in C++
is prefix while it is a postfix operator in EBNF. This is true for the other
operators in Syntx as well.

Another operator that is used in the example above is that of optionality:
``?`` in EBNF, ``!`` in Syntx (as the closest symbol to a question mark).

There's a third operator, the negative sign operator, that appears only in the
Syntx listing. This is because EBNF descriptions are usually given at token
level and whitespaces are usually ignored. In Syntx one needs to deal with
whitespaces, but it is made as easy as possible: the negative signs added to
any rule (simple or compound) will "eat" all the whitespaces before the rule is
matched.

The ``real``, ``character``, ``keyword`` and ``string`` are built-in types of
the framework. They are instantiated as temporaries in rule definitions with
the necessary arguments. As to what they are used for, their names are pretty
much self-explanatory.

The ``epsilon`` type might need a little explanation though. It is a rule that
always matches but doesn't consume anything from the input text. It is only
needed in special cases, for example in the first line of the example (in rule
``json``) where, used together with the negative sign, it consumes all the
whitespaces from after the ``object``. It is needed here because for Syntx a
successful match is not enough: it needs a full match. This means that the
match needs to be successful and it needs to cover the entire input text. Not
even spaces and newlines can remain after the last matched character.

Actually, the grammar above could have been given in an even more concise
manner in Syntx. There is a phrase that appears very often in grammars. It is
the way separated lists can be expressed. There are two examples of such in the
JSON grammar, one is given here:

```cpp
-value << *(-character{","} << -value)
```

This means that a value needs to match and afterwards zero or more commas and
values come in a sequence. This can be expressed in Syntx as:

```cpp
-value % -character{","}
```

The ``%`` operator can't be found in EBNF. It means that its first operand
needs to match at least once followed by an optional sequence of the second
operand and then the first operand.

The result of parsing in Syntx is not just a boolean value saying whether there
was a successful match. An AST is also produced when a match succeeds. It is
little bit unconventional, different from the AST's usually shown in textbooks.
It's a more useful one that's easier to work with and the structure of which can
be easily modified by the way the grammar is given.

For usage examples, please check out the tests provided in ``test/tests.cc``.

Before delving into the details, it is worth mentioning that this is the third version
of the Syntx library. The previous versions have all been published on Gitlab:

- _SyntX:_ [https://gitlab.com/nagygr/syntx](https://gitlab.com/nagygr/syntx) 
- _Syntx II:_ [https://gitlab.com/nagygr/syntxii](https://gitlab.com/nagygr/syntxii)

The second version is almost identical to the third one in terms of API. The
main difference is that it uses inheritence for the polymorphic handling of
rules (mainly by the operators) and thus uses the heap a lot, the third edition
is based on template operators and is more stack intensive.

The second version contains a very detailed documentation in PDF format. Look
there for an insight into the internals and philosophy behind Syntx.

### Using Syntx as an external project

#### Bazel
If you're building your application with Bazel, you simply need to add a few
lines to both your WORKSPACE and BUILD files. As Syntx itself has Bazel build
files as well, it can be included as an external Bazel project which makes
things very simple.

Add the following lines to the Bazel WORKSPACE file:

```
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

http_archive(
	name = "syntxiii",
	url = "https://gitlab.com/nagygr/syntxiii/-/archive/master/syntxiii-master.zip",
	strip_prefix = "syntxiii-master",
)
```

and the following lines to the BUILD file of the executable that depends on
Syntx:

```
cc_binary(
	...
	deps = ["@syntxiii//:syntx"],
	copts =
	[
		"-std=c++17"
	],
	linkstatic=False
)
```

#### CMAKE
With CMAKE you can create a subdirectory where you download and compile Syntx
and then link to the library.

Add ``cmake/syntxiii.cmake`` to the project with the following contents:

```
include(FetchContent)

FetchContent_Populate(
    syntx
    GIT_REPOSITORY https://gitlab.com/nagygr/syntxiii.git
)

add_subdirectory(${syntx_SOURCE_DIR} ${syntx_BINARY_DIR})
```

In the project's main ``CMakeLists.txt`` file add the following lines:

```
include(cmake/syntxiii.cmake)

add_executable(${EXECUTABLE_NAME} ${SOURCES})

target_link_libraries(${EXECUTABLE_NAME} syntx)
target_include_directories(${EXECUTABLE_NAME} PRIVATE ${syntx_SOURCE_DIR}/lib/src)

set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)
```

The variables ``EXECUTABLE_NAME``, ``SOURCES`` and ``PROJECT_NAME`` are
project-specific, set them accordingly or use your own values instead.

The last line above sets the C++ standard to be used to C++17. This is the
minimum required for Syntx, but newer versions can of course be set.

## The operators of Syntx
The following table summarizes the operators of the framework:

| Operator       | Definition                                                |
|:--------------:|-----------------------------------------------------------|
| ``lhs`` \| ``rhs``  | _Alternation_: either ``lhs`` or ``rhs`` matches          |
| ``lhs << rhs`` | _Concatenation_: first ``lhs`` then ``rhs`` matches       |
| ``lhs % rhs``  | _Separated list_: first ``lhs`` matches then a sequence of ``rhs`` and ``lhs`` arbitrary times |
| ``!op``        | _Option_: ``op`` either matches or not                    |
| ``+op``        | _Repetition_: ``op`` matches at least once                |
| ``*op``        | _Repetition_ _or_ _epsilon_: ``op`` can match any times   |
| ``-op``        | whitespaces are consumed before ``op``                    |
| ``~op``        | whitespaces but _not newline_ are consumed before ``op``  |

## The Syntx ecosystem
There are a few objects that need to be instantiated and used to make parsing
possible:

- _Context:_ the context is the data structure that keeps note of the input
  text that the parser works with. It is actually just a tuple containing two
  iterators: one pointing at the next character to be processed, the other at
  the end of the text. The type of the context is: ``match_range``.
- _Result_: the matched interval of a rule is also returned in a
  ``match_range`` structure. The one returned by the top level rule should
  cover the entire text.
- _Root:_ the root of the AST (abstract Syntx Tree) that is created during
  parsing. The type is ``node`` and it created on the heap and should be
  guarded by a ``shared_ptr``.
- _Error:_ the framework constructs an error message giving the most possible
  information helping the users to find syntax errors in the input texts. It's
  type is ``syntax_error``.

Parsing can be initiated using the ``parse`` function which takes the following operands:

- _Top level rule_
- _Context_
- _Result_
- _Root_
- _Error_

Here is a full, working example:

```cpp
namespace sx = h119::syntx;
namespace sp = h119::syntx::parser;

std::string_view text = "a";
sx::match_range context{text.begin(), text.end()}, result;
auto root = std::make_shared<sx::node>("root");
sx::syntax_error error;

auto r = sp::character{"a"};

if (sp::parse(r, context, result, root, error))
{
	std::cout << "Passed" << std::endl;
	std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;
	return true;
}
else
{
	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}
```

The example also shows the actual namespace of the mentioned types and the way
the results can be used after a successful and a failed parse.

The matched text range can be converted back to a string using the ``string`` type's constructor:

```cpp
std::string{std::get<0>(result), std::get<1>(result)}
```

The error message needs to be constructed from the ``syntx_error`` struct using
the ``error_message`` function which returns a string:

```cpp
sx::error_message(context, error)
```

## Error messages
As almost every real-life grammar contains alternatives, where only one of the
possibly many options can match, rules will fail throughout parsing even in
case of a completely valid input file. Of course, the framework is able to tell
whether the output is OK even if one the rules fails. It is quiet easy
actually: if the top level rule succeeds then the framework could find a
parsing route that covers the entire input.

What makes creating good error messages hard is that the exact location of the
error and the expected set of characters is only known at lowest level, when a
rule fails, but at that point it is impossible to say whether that will cause
the entire parsing to fail or not. That can only be known at the topmost level.

Syntx tackles this problem and provides an error message that gives:

- the _exact position_ of the error,
- the _set of characters/identifiers etc._ that were expected but not found,
- the _name of the actual rule_ that failed.

As to the third point: rules can have names in Syntx and they are also used in AST's.

Let's analyse the following example:

```cpp
namespace sx = h119::syntx;

std::string_view text = 
R"foo(
	{
	  "firstName" "John"
	}
)foo";

sx::match_range context{text.begin(), text.end()}, result;
auto root = std::make_shared<sx::node>("root");
sx::syntax_error error;
h119::syntx::parser::rule json{"json"}, value{"value"}, key_value{"key_value"}, object{"object"}, array{"array"};

{
	using namespace h119::syntx::parser;

	json = value << -epsilon{};
	value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
	object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
	key_value = -string{} << -character{":"} << -value;
	array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
}

if (h119::syntx::parser::parse(json, context, result, root, error))
{
	std::cout << "Passed" << std::endl;
	std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

	return false;
}

std::cout << "Failed" << std::endl;
std::cout << sx::error_message(context, error) << std::endl;
```

The example is an invalid JSON object: the colon (_:_) is missing from between
the key and the value.In response to this input, Syntx gives the following
error message (please note that the rules have names -- they get it upon construction):

```
A character from the character set: ":" failed while trying to match a(n) key_value:
		{
		  "firstName" 
		  		              "
		  		               John"
		}
	
		  		              ^
```

Please note, that the line where the error was found is broken into three parts
in the error message:

1. the part before the error
2. the character where the error occured (marked by the ^ character below the text)
3. the part after the error

If this error message doesn't suit the needs of a certain application the core
type, that contains all the information that is used to build this message, can
be used. It is called ``syntax_error``.

The ``syntax_error`` data structure is a tuple and looks like this:

```cpp
using syntax_error = std::tuple<std::string, iterator_type, std::string_view>;
```

The fields are the following:

1. _character level error message:_ the low level information about what was expected (here: A character from the character set: ":")
2. _the error position:_ the iterator pointing to the character where something unexpected has been found
3. _the name of the rule:_ the (named) rule that was begin parsed when the error occured

If the error occurs while parsing an unnamed rule, the algorithm will find the
first rule upwards in the hierarchy that actually has a name. As to how and
when the rules should be named please refer to the section about the AST's.

## Abstract Syntax Trees
It has already been mentioned that Syntx doesn't build conventional AST's. To make it easy to understand how the provided AST is
different, first let's look at a conventional one. The example is the JSON grammar we've already seen earlier:

```cpp
h119::syntx::parser::rule json{}, value{"value"}, key_value{}, object{"object"}, array{"array"};

json = object << -epsilon{};
value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
key_value = -string{} << -character{":"} << -value;
array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
```

Let the input text be very simple:

```
{
	"name": "Mickey Mouse",
	"date_of_birth": 1928
}
```

The textbook AST for this text would look like:

```
json
+--- value
     +--- object
          +--- "{"
          +--- key_value
     	      +--- "name"
     		  +--- ":"
     		  +--- "Mickey Mouse"
     	 +--- ","
     	 +--- key_value
     	      +--- "date_of_birth"
     		  +--- ":"
     		  +--- 1928
          +--- "}"
```

For such a short input any tree is relatively easy to parse, and the
differences to the actual AST might seem small at first, but they become bugger
as the AST gets more copmlex.

There are two big problems with the AST above:

1. there are many elements in it that noone's interested in -- they are only
needed for syntactial reasons, or simply to make the text more readable. For
example the '{', '}', ':' and ',' characters. These will not be processed by
the algorithm that uses a JSON input.
2. the actual values are deep down in the tree structure.

The AST created by Syntx tackles both problems. The solution to the first one
is that the terminal (character-level) symbols are not included at all in the
tree. The only elements of the grammar that actually appear in the AST are the
rules that have a name (e.g. the ``value`` rule will be present but
``json`` and ``key_value`` will not): 

```
root
+--- value
     +--- object
          +--- value
          +--- value
```

The AST is much more concise now but the values seem to be completely left out
which makes it completely useless. Actually this is only half of the
information that is included in Syntx's AST's: every node in the AST contains
two iterators: the interval within the input text that was matched by the given
rule. This way the matches are accessible at any level, the tree traversal can
stop at any point where the needed information is already present.

So actually, if we show the values as well next to the rule names, the tree
looks like this:

```
root  (   {    "name": "Mickey Mou...)
+--- value  (   {    "name": "Mickey Mou...)
     +--- object  (   {    "name": "Mickey Mou...)
          +--- value  ("Mickey Mouse")
          +--- value  (1928)
```

The tree is as informative as possible and also very compact. Although probably
not the best one for processing JSON files. The keys are missing and it would
actually be good to know the type of the values as well -- that can make
processing the contents much easier.

Please note that there is an extra node in the AST's of Syntx: the ``root``
node. This is a consequence of how the AST's are built by the framework.

The grammar used to create the AST's above was probably the most compact
grammar that can describe a JSON file. It was a good example to show how simple
it is to define grammars using Syntx. In a real-life situation one would
probably create a little bit bigger grammar to get a better AST. Let's look at
the following grammar:

```cpp
h119::syntx::parser::rule
	json{}, value{"value"}, key_value{}, key{"key"},
	object{"object"}, array{"array"}, null_keyword{"null"},
	boolean_value{"boolean_value"}, number{"number"}, string_type{"string"};


json = value << -epsilon{};

object = -character{"{"} << !(key_value % -character{","}) << -character{"}"};

key = string{}; 
value = number | string_type | null_keyword | boolean_value | object | array;

array = -character{"["} << !(-value % -character{","}) << -character{"]"};

key_value = -key << -character{":"} << -value;

null_keyword = keyword{"null"};
boolean_value = keyword{"true"} | keyword{"false"};
number = real{};
string_type = string{};
```

Please note, that there are a few new rules:

1. ``key``: to have a separate node containing the keys (field names) of an object
2. ``null_keyword``,``boolean_value``, ``number``, ``string_type``: a rule for every type will make processing easier

The type rules can be used to make sure that the type of a values is what was
expected. If the type node gives what we expect then the value string can be
fed to a converter function without the need to prepare for error handling.

Let's take a look at the tree we have now:

```
root  (   {    "name": "Mickey Mou...)
+--- value  (   {    "name": "Mickey Mou...)
     +--- object  (   {    "name": "Mickey Mou...)
          +--- key  ("name")
          +--- value  ("Mickey Mouse")
               +--- string  ("Mickey Mouse")
          +--- key  ("date_of_birth")
          +--- value  (1928)
               +--- number  (1928)
```

The advantages of the grammar above are more evident when the input is more complex:

```
{
  "firstName": "John",
  "isAlive": true,
  "age": 27,
  "address": {
	"streetAddress": "21 2nd Street",
	"city": "New York"
  },
  "phoneNumbers": [
	{
	  "type": "home",
	  "number": "212 555-1234"
	},
	{
	  "type": "office",
	  "number": "646 555-4567"
	}
  ],
  "children": [],
  "spouse": null
}
```

The tree for this output is (the values are not shown for better readability):

```
root
+--- json
     +--- value
          +--- object
               +--- key
               +--- value
                    +--- string
               +--- key
               +--- value
                    +--- boolean_value
               +--- key
               +--- value
                    +--- number
               +--- key
               +--- value
                    +--- object
                         +--- key
                         +--- value
                              +--- string
                         +--- key
                         +--- value
                              +--- string
               +--- key
               +--- value
                    +--- array
                         +--- value
                              +--- object
                                   +--- key
                                   +--- value
                                        +--- string
                                   +--- key
                                   +--- value
                                        +--- string
                         +--- value
                              +--- object
                                   +--- key
                                   +--- value
                                        +--- string
                                   +--- key
                                   +--- value
                                        +--- string
               +--- key
               +--- value
                    +--- array
               +--- key
               +--- value
                    +--- null
```

The data structure that holds a node is called: ``node``:

```cpp
struct node
{
	auto matching_range() const -> match_range {...}

	...

	std::string_view name;
	std::vector<std::shared_ptr<node>> children;
};
```

The ``name`` field contains the the name of the rule that the node represents,
the ``matching_range()`` method returns that range that is matched by the given
rule and ``children`` contains the children of the node.
