.phony: init all run_tests run_formulae run clean tarball source_files csc gtags tags doc

TEST_TARGET=syntx_test
FORMULAE_TARGET=syntx_formulae
SOURCE_DIRECTORY=lib/src
TEST_DIRECTORY=test
TEST_DIRECTORY=test
FORMULAE_DIRECTORY=formulae
BUILD_DIRECTORY=build
#BUILD_TYPE=DEBUG # or: RELEASE
BUILD_TYPE=RELEASE

# DO NOT MODIFY THE FILE BELOW THIS LINE (UNLESS YOU KNOW WHAT YOU ARE DOING)

init:
	mkdir -p $(BUILD_DIRECTORY) && ln -fs $(BUILD_DIRECTORY)/$(TEST_TARGET) $(TEST_TARGET) && ln -fs  $(BUILD_DIRECTORY)/$(FORMULAE_TARGET) $(FORMULAE_TARGET)

all: init
	cd $(BUILD_DIRECTORY) && cmake -DCMAKE_BUILD_TYPE:STRING=$(BUILD_TYPE)  .. && make

run_tests: all
	./$(TEST_TARGET)

run_formulae: all
	./$(FORMULAE_TARGET)

run: all
	 ./$(TEST_TARGET) && ./$(FORMULAE_TARGET)

tarball: clean
	tar cvzf $(TEST_TARGET)_`date +"%Y%m%d_%H%M"`.tgz *
	
clean:
	rm -rf $(BUILD_DIRECTORY)
	rm -f $(TEST_TARGET)
	rm -f $(FORMULAE_TARGET)
	rm -rf GPATH GRTAGS GTAGS cscope.*

source_files:
	find $(SOURCE_DIRECTORY) $(TEST_DIRECTORY) $(FORMULAE_DIRECTORY) -name "*.h" -o -name "*.cc" > cscope.files

csc: source_files
	cscope -Rb

gtags: source_files
	gtags --accept-dotfiles -c -f cscope.files

tags: csc gtags

doc: 
	lowdown -s -o README.html README.md
