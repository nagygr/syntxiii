#include <iostream>
#include <functional>
#include <vector>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/alternation.h>
#include <h119/syntx/parser/character.h>
#include <h119/syntx/parser/concatenation.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/syntx/parser/rule.h>
#include <h119/syntx/parser/epsilon.h>
#include <h119/syntx/parser/identifier.h>
#include <h119/syntx/parser/in_range.h>
#include <h119/syntx/parser/not_in_range.h>
#include <h119/syntx/parser/not_character.h>
#include <h119/syntx/parser/integer.h>
#include <h119/syntx/parser/real.h>
#include <h119/syntx/parser/keyword.h>
#include <h119/syntx/parser/option.h>
#include <h119/syntx/parser/repetition.h>
#include <h119/syntx/parser/repetition_or_epsilon.h>
#include <h119/syntx/parser/whitespace.h>
#include <h119/syntx/parser/whitespace_not_newline.h>
#include <h119/syntx/parser/string.h>
#include <h119/syntx/parser/substring.h>
#include <h119/syntx/parser/separated_list.h>

auto simple_character() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "a";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;

	auto r = sp::character{"a"};

	if (sp::parse(r, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;
		return true;
	}
	else
	{
		std::cout << "Failed" << std::endl;
		std::cout << sx::error_message(context, error) << std::endl;
		return false;
	}
}

auto simple_concatenation() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "ab";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;

	auto r = sp::character{"a"} << sp::character{"b"};

	if (sp::parse(r, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_rule_without_name() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "ab";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1, r2;

	r1 = sp::character{"a"} << r2;
	r2 = sp::character{"b"};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_rule_with_name() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "ab";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"r1"}, r2{"r2"};

	r1 = sp::character{"a"} << r2;
	r2 = sp::character{"b"};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_rule_with_error() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "ab";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"r1"}, r2{"r2"};

	r1 = sp::character{"awe"} << r2;
	r2 = sp::character{"cde"};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return false;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return true;
}

auto simple_alternation() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "ab";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"r1"}, r2, r3{"r3"}, r4{"r4"};

	r1 = sp::character{"aop"} << r2;
	r2 = r3 | r4;
	r3 = sp::character{"fgh"};
	r4 = sp::character{"bcd"};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_epsion() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "abccbaabb";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule head{"head"}, repeat{"repeat"};

	head = repeat | sp::epsilon{};
	repeat = sp::character{"abc"} << head;

	if (sp::parse(head, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_identifier() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "my_variable9__";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule an_identifier{"an_identifier"};

	an_identifier = sp::identifier{};

	if (sp::parse(an_identifier, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_identifier_failure() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "3my_variable9__";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule an_identifier{"an_identifier"};

	an_identifier = sp::identifier{};

	if (sp::parse(an_identifier, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;
		return false;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return true;
}

auto simple_in_range() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "abccbaabb";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule head{"head"}, repeat{"repeat"};

	head = repeat | sp::epsilon{};
	repeat = sp::in_range{'a', 'c'} << head;

	if (sp::parse(head, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_not_in_range() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "abccbaabb";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule head{"head"}, repeat{"repeat"};

	head = repeat | sp::epsilon{};
	repeat = sp::not_in_range{'d', 'g'} << head;

	if (sp::parse(head, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_not_character() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "abccbaabb";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule head{"head"}, repeat{"repeat"};

	head = repeat | sp::epsilon{};
	repeat = sp::not_character{"defg"} << head;

	if (sp::parse(head, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_integer() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "-356721";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule number{"number"};

	number = sp::integer{};

	if (sp::parse(number, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_reals() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "-356721 1.2 -23.32 34.134e2 -12.1342e-4 32e+33 +32.123e+231 ";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule number{"number"};

	number = (sp::real{} << sp::character{" "} << number) | sp::epsilon{};

	if (sp::parse(number, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_keyword() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "for_each1";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule a_keyword{"a_keyword"};

	a_keyword = sp::keyword{"for_each1"};

	if (sp::parse(a_keyword, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_keywords() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "apple*orange*my_fruits ";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule keywords{"keywords"};

	keywords = sp::keyword{"apple"} << sp::character{"*+!"} << sp::keyword{"orange"} << sp::character{"*%\""} << sp::keyword{"my_fruits"} << sp::character{" "};

	if (sp::parse(keywords, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_option() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "123";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1;

	r1 = !sp::integer{} << !(sp::character{","} << sp::integer{});

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;

}

auto simple_repetition() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "12 23 43 78 ";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"numbers"};

	r1 = +(sp::integer{} << sp::character{" "});

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_repetition_or_epsilon() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "12 23 43 82 ";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"numbers"};

	r1 = *sp::character{"qwerty"} << *(sp::integer{} << sp::character{" "});

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_whitespace() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "-12 53 +32 82 ";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1{"numbers"};

	r1 = +-sp::integer{} << -sp::epsilon{};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_whitespace_not_newline() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "-12 53 \t\t+32    82\t \n233 \t23\t\t23\n1";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule l{"line"}, n{"numbers"};

	l = +n;
	n = +~sp::integer{} << -sp::epsilon{};

	if (sp::parse(l, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_string() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "\"Hello \\\"world\\\"!\"";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1;

	r1 = sp::string{};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto simple_substring() -> bool
{
	namespace sx = h119::syntx;
	namespace sp = h119::syntx::parser;

	std::string_view text = "my_int";
	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	sp::rule r1;

	r1 = sp::substring{"my_"} << sp::identifier{};

	if (sp::parse(r1, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_example() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
		  "firstName": "John",
		  "lastName": "Smith",
		  "isAlive": true,
		  "age": 27,
		  "address": {
			"streetAddress": "21 2nd Street",
			"city": "New York",
			"state": "NY",
			"postalCode": "10021-3100"
		  },
		  "phoneNumbers": [
			{
			  "type": "home",
			  "number": "212 555-1234"
			},
			{
			  "type": "office",
			  "number": "646 555-4567"
			},
			{
			  "type": "mobile",
			  "number": "123 456-7890"
			}
		  ],
		  "children": [],
		  "spouse": null
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule json{"json"}, value{"value"}, key_value{"key_value"}, object{"object"}, array{"array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};
		value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
		object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
		key_value = -string{} << -character{":"} << -value;
		array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_example_error() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
		  "firstName": "John"
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule json{"json"}, value{"value"}, key_value{"key_value"}, object{"object"}, array{"array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value; // a "<< -epsilon{}" is missing from here -- this is a test to show the error message indicating a partial match
		value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
		object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
		key_value = -string{} << -character{":"} << -value;
		array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return false;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return true;
}

auto json_example_syntax_error() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
		  "firstName" "John"
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule json{"json"}, value{"value"}, key_value{"key_value"}, object{"object"}, array{"array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};
		value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
		object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
		key_value = -string{} << -character{":"} << -value;
		array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root);
		return false;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return true;
}

auto json_example_tree() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
		  "firstName": "John",
		  "isAlive": true,
		  "age": 27,
		  "address": {
			"streetAddress": "21 2nd Street",
			"city": "New York"
		  },
		  "phoneNumbers": [
			{
			  "type": "home",
			  "number": "212 555-1234"
			},
			{
			  "type": "office",
			  "number": "646 555-4567"
			}
		  ],
		  "children": [],
		  "spouse": null
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule
		json{"json"}, value{"value"}, key_value{}, key{"key"},
		object{"object"}, array{"array"}, null_keyword{"null"},
		boolean_value{"boolean_value"}, number{"number"}, string_type{"string"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};

		object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};

		key = string{}; 
		value = number | string_type | null_keyword | boolean_value | object | array;
		
		array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
		
		key_value = -key << -character{":"} << -value;
		
		null_keyword = keyword{"null"};
		boolean_value = keyword{"true"} | keyword{"false"};
		number = real{};
		string_type = string{};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		std::cout << "The AST:" << std::endl << "********" << std::endl;
		sx::print_tree(std::cout, root, sx::tree_verbosity::concise);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_separated_list() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
		  "firstName": "John",
		  "isAlive": true,
		  "age": 27,
		  "address": {
			"streetAddress": "21 2nd Street",
			"city": "New York"
		  },
		  "phoneNumbers": [
			{
			  "type": "home",
			  "number": "212 555-1234"
			},
			{
			  "type": "office",
			  "number": "646 555-4567"
			}
		  ],
		  "children": [],
		  "spouse": null
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule
		json{"json"}, value{"value"}, key_value{}, key{"key"},
		object{"object"}, array{"array"}, null_keyword{"null"},
		boolean_value{"boolean_value"}, number{"number"}, string_type{"string"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};

		object = -character{"{"} << !(key_value % -character{","}) << -character{"}"};

		key = string{}; 
		value = number | string_type | null_keyword | boolean_value | object | array;
		
		array = -character{"["} << !(-value % -character{","}) << -character{"]"};
		
		key_value = -key << -character{":"} << -value;
		
		null_keyword = keyword{"null"};
		boolean_value = keyword{"true"} | keyword{"false"};
		number = real{};
		string_type = string{};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		sx::print_tree(std::cout, root, sx::tree_verbosity::no_value);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_simple_example_tree() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
			"name": "Mickey Mouse",
			"date_of_birth": 1928
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule json{}, value{"value"}, key_value{}, object{"object"}, array{"array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};
		value = real{} | string{} | keyword{"null"} | keyword{"true"} | keyword{"false"} | object | array;
		object = -character{"{"} << !(-key_value << *(-character{","} << -key_value)) << -character{"}"};
		key_value = -string{} << -character{":"} << -value;
		array = -character{"["} << !(-value << *(-character{","} << -value)) << -character{"]"};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		std::cout << "The AST:" << std::endl << "********" << std::endl;
		sx::print_tree(std::cout, root, sx::tree_verbosity::concise);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_simple_example_better_grammar_tree() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		{
			"name": "Mickey Mouse",
			"date_of_birth": 1928
		}
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule
		json{}, value{"value"}, key_value{}, key{"key"},
		object{"object"}, array{"array"}, null_keyword{"null"},
		boolean_value{"boolean_value"}, number{"number"}, string_type{"string"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};

		object = -character{"{"} << !(key_value % -character{","}) << -character{"}"};

		key = string{}; 
		value = number | string_type | null_keyword | boolean_value | object | array;
		
		array = -character{"["} << !(-value % -character{","}) << -character{"]"};
		
		key_value = -key << -character{":"} << -value;
		
		null_keyword = keyword{"null"};
		boolean_value = keyword{"true"} | keyword{"false"};
		number = real{};
		string_type = string{};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		std::cout << "The AST:" << std::endl << "********" << std::endl;
		sx::print_tree(std::cout, root, sx::tree_verbosity::concise);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_value_safe_array() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		[
			[
				{
					"name": "Ted",
					"age": 26
				},
				{
					"name": "Barney",
					"age": 28
				}
			],
			[
				null,
				"hello",
				"what up"
			]
		]
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule
		json{}, value{"value"}, key_value{}, key{"key"},
		object{"object"}, array{}, null_keyword{"null"},
		boolean_value{"boolean_value"}, number{"number"}, string_type{"string"},
		number_array{"number array"}, boolean_array{"boolean_array"},
		string_array{"string_array"}, object_array{"object_array"},
		array_array{"array_array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};

		object = -character{"{"} << !(key_value % -character{","}) << -character{"}"};

		key = string{}; 
		value = number | string_type | null_keyword | boolean_value | object | array;
		
		array = number_array | boolean_array | string_array | object_array | array_array;

		number_array = -character{"["} << !((-number | -null_keyword) % -character{","}) << -character{"]"};
		boolean_array = -character{"["} << !((-boolean_value | -null_keyword) % -character{","}) << -character{"]"};
		string_array = -character{"["} << !((-string_type | -null_keyword) % -character{","}) << -character{"]"};
		object_array = -character{"["} << !((-object | -null_keyword) % -character{","}) << -character{"]"};
		array_array = -character{"["} << !((-array | -null_keyword) % -character{","}) << -character{"]"};
		
		key_value = -key << -character{":"} << -value;
		
		null_keyword = keyword{"null"};
		boolean_value = keyword{"true"} | keyword{"false"};
		number = real{};
		string_type = string{};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		std::cout << "The AST:" << std::endl << "********" << std::endl;
		sx::print_tree(std::cout, root, sx::tree_verbosity::concise);
		return true;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return false;
}

auto json_value_safe_array_failure_test() -> bool
{
	namespace sx = h119::syntx;

	std::string_view text = 
	R"foo(
		[
			[
				{
					"name": "Ted",
					"age": 26
				},
				"Hello world!"
			],
			[
				null,
				12,
				34.3
			]
		]
	)foo";

	sx::match_range context{text.begin(), text.end()}, result;
	auto root = std::make_shared<sx::node>("root");
	sx::syntax_error error;
	h119::syntx::parser::rule
		json{}, value{"value"}, key_value{}, key{"key"},
		object{"object"}, array{}, null_keyword{"null"},
		boolean_value{"boolean_value"}, number{"number"}, string_type{"string"},
		number_array{"number array"}, boolean_array{"boolean_array"},
		string_array{"string_array"}, object_array{"object_array"},
		array_array{"array_array"};
	
	{
		using namespace h119::syntx::parser;
	
		json = value << -epsilon{};

		object = -character{"{"} << !(key_value % -character{","}) << -character{"}"};

		key = string{}; 
		value = number | string_type | null_keyword | boolean_value | object | array;
		
		array = number_array | boolean_array | string_array | object_array | array_array;

		number_array = -character{"["} << !((-number | -null_keyword) % -character{","}) << -character{"]"};
		boolean_array = -character{"["} << !((-boolean_value | -null_keyword) % -character{","}) << -character{"]"};
		string_array = -character{"["} << !((-string_type | -null_keyword) % -character{","}) << -character{"]"};
		object_array = -character{"["} << !((-object | -null_keyword) % -character{","}) << -character{"]"};
		array_array = -character{"["} << !((-array | -null_keyword) % -character{","}) << -character{"]"};
		
		key_value = -key << -character{":"} << -value;
		
		null_keyword = keyword{"null"};
		boolean_value = keyword{"true"} | keyword{"false"};
		number = real{};
		string_type = string{};
	}

	if (h119::syntx::parser::parse(json, context, result, root, error))
	{
		std::cout << "Passed" << std::endl;
		std::cout << std::string{std::get<0>(result), std::get<1>(result)} << std::endl;

		std::cout << "The AST:" << std::endl << "********" << std::endl;
		sx::print_tree(std::cout, root, sx::tree_verbosity::concise);
		return false;
	}

	std::cout << "Failed" << std::endl;
	std::cout << sx::error_message(context, error) << std::endl;
	return true;
}

auto main() -> int
{
	std::vector<std::function<auto()->bool>> tests =
	{
		simple_character,
		simple_concatenation,
		simple_rule_without_name,
		simple_rule_with_name,
		simple_rule_with_error,
		simple_alternation,
		simple_epsion,
		simple_identifier,
		simple_identifier_failure,
		simple_in_range,
		simple_not_in_range,
		simple_not_character,
		simple_integer,
		simple_reals,
		simple_keyword,
		simple_keywords,
		simple_option,
		simple_repetition,
		simple_repetition_or_epsilon,
		simple_whitespace,
		simple_whitespace_not_newline,
		simple_string,
		simple_substring,
		json_example,
		json_example_error,
		json_example_syntax_error,
		json_example_tree,
		json_separated_list,
		json_simple_example_tree,
		json_simple_example_better_grammar_tree,
		json_value_safe_array,
		json_value_safe_array_failure_test
	};

	std::size_t number = 0, failed = 0;
	for (auto test: tests)
	{
		++number;

		std::cout << "##############################################" << std::endl;
		std::cout << "# Running test no. " << number << std::endl;
		std::cout << "##############################################" << std::endl;

		if (test())
		{
			std::cout << "Succeeded (no. " << number << ")" << std::endl;
		}
		else
		{
			std::cout << "Failed (no. " << number << ")"  << std::endl;
			++failed;
		}

		std::cout << "==============================================" << std::endl;
	}

	std::cout << "##############################################" << std::endl;
	std::cout << "# TEST SUMMARY: " << (number - failed) << "/" << number << " passed ";
	if (failed > 0) std::cout << " (" << failed << " failed)";
	std::cout << std::endl;
	std::cout << "##############################################" << std::endl;
}
