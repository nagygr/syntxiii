#pragma once

#include <cstddef>         // size_t
#include <memory>          // std::shared_ptr
#include <vector>          // std::vector
#include <ostream>         // std::ostream
#include <iostream>        // std::cout
#include <string>          // std::string
#include <string_view>     // std::string_view

#include <h119/syntx/core_types.h>
#include <h119/util/string/logger.h>

namespace h119::syntx
{

struct node
{
	using node_iterator = std::vector<std::shared_ptr<node>>::const_iterator;

	node(std::string_view name):
		name(name),
		matching_range_set_{false}
	{}

	node(std::string_view name, match_range matching_range):
		name(name),
		matching_range_set_{true},
		matching_range_(matching_range)
	{}

	auto matching_range(match_range range) -> node &
	{
		matching_range_ = range;
		log_5
		(
			"Matching range has been set in node %0: %1",
			name,
			std::string
			{
				std::get<0>(matching_range_),
				std::get<1>(matching_range_)
			}
		);
		matching_range_set_ = true;
		return *this;
	}

	auto matching_range() const -> match_range
	{
		/*
		 * This log entry can be used to check the value of the matching_range_ member. 
		 * //log_5("Node (%0) inner match range: %1", name, std::string(matching_range_.first, matching_range_.second));
		 */
		if (matching_range_set_)
			return matching_range_;
		else 
		{
			std::size_t size = children.size();

			if (size == 0) return match_range();
			
			return match_range
			(
				std::get<0>(children[0]->matching_range_),
				std::get<1>(children[size - 1]->matching_range_)
			);
		}
	}

	auto cbegin() const -> node_iterator
	{
		return children.cbegin();
	}

	auto cend() const -> node_iterator
	{
		return children.cend();
	}
	
	std::string name;
	std::vector<std::shared_ptr<node>> children;

	private:
		bool matching_range_set_;
		match_range matching_range_;
};

enum class tree_verbosity
{
	verbose,
	concise,
	no_value
};

auto print_tree(std::ostream &os, std::shared_ptr<node> root, tree_verbosity verbosity = tree_verbosity::verbose, size_t level = 0) -> void;
auto create_dot_tree(std::shared_ptr<node> root, std::ostream &stream = std::cout) -> void;

}
