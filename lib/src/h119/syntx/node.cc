#include <cstddef>
#include <sstream>         // std::stringstream
#include <iterator>        // std::distance
#include <algorithm>       // std::replace

#include <h119/syntx/node.h>

namespace h119::syntx
{

auto clamp_value_text(match_range matching_range, bool verbose, std::size_t max_length = 20) -> std::string;

auto print_tree
(
	std::ostream &os,
	std::shared_ptr<node> root,
	tree_verbosity verbosity, 
	size_t level
) -> void
{
	if (root)
	{
		char const *empty = "     ";
		char const *arm   = "+--- ";

		if (level > 0)
		{
			for (size_t i = 0; i < level - 1; ++i) os << empty;
			os << arm;
		}

		if (verbosity == tree_verbosity::no_value)
			os << root->name << std::endl;
		else
		{
			match_range range = root->matching_range();
			std::string value_text = clamp_value_text(range, verbosity == tree_verbosity::verbose, 30);

			os << root->name << "  (" << value_text << ")" << std::endl;
		}

		for (auto &child: root->children) print_tree(os, child, verbosity, level + 1);
	}
}

auto create_dot_tree_helper(std::shared_ptr<node> root, std::ostream &stream) -> void;

auto create_dot_tree(std::shared_ptr<node> root, std::ostream &stream) -> void
{
	std::string preamble =
	R"raw(
	digraph G {
		ranksep=0.25;
		graph [ bgcolor=transparent ]
		node [ style=filled, fillcolor=white, fontname="Sans", fontsize=12, margin=0.025 ]
	)raw";

	std::string closing_mark = "}\n";

	stream << preamble;

	create_dot_tree_helper(root, stream);

	stream << closing_mark;
}

auto create_dot_tree_helper(std::shared_ptr<node> root, std::ostream &stream) -> void
{
	match_range the_matching_range = root->matching_range();
	std::string value_text = clamp_value_text(the_matching_range, false);

	for (auto &c: value_text)
	{
		if (c == '\n' || c == '\t') c = ' ';
		if (c == '"' || c == '\'') c = '`';
	}

	std::stringstream name, value;
	name << "l" << root.get();
	value << root->name << "\\n" << value_text;

	stream << name.str() << " [label = \"" << value.str() << "\"]" << std::endl;

	for (auto const &child: root->children)
	{
		std::stringstream child_name;
		child_name << "l" << child.get();
		stream << name.str() << " -> " << child_name.str() << std::endl;
	}

	for (auto &child: root->children)
		create_dot_tree_helper(child, stream);
}

auto clamp_value_text(match_range matching_range, bool verbose, std::size_t max_length) -> std::string
{

	std::string value_text;

	if (!verbose && std::distance(std::get<0>(matching_range), std::get<1>(matching_range)) > static_cast<long int>(max_length))
	{
		value_text = std::string(std::get<0>(matching_range), std::get<0>(matching_range) + max_length);
		if (max_length >= 3)
			value_text[max_length - 1] = value_text[max_length - 2] = value_text[max_length - 3] = '.';

		std::replace(value_text.begin(), value_text.end(), '\n', ' ');
		std::replace(value_text.begin(), value_text.end(), '\t', ' ');
	}
	else value_text = std::string(std::get<0>(matching_range), std::get<1>(matching_range));

	return value_text;
}

}
