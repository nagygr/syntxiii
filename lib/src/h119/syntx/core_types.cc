#include <cstdlib>         // std::abs
#include <iterator>        // std::distance
#include <sstream>         // std::stringstream
#include <fstream>         // std::fstream
#include <stdexcept>       // std::runtime_error

#include <h119/util/string/format.h>
#include <h119/syntx/core_types.h>

namespace h119::syntx
{

auto error_message(match_range const &context, syntax_error const &error) -> std::string 
{
	constexpr int offset = 20;

	std::string character_level_error_name = std::get<0>(error);
	std::string rule_level_error_name = std::get<2>(error);

	if (character_level_error_name == "" || rule_level_error_name == "")
		return "Empty error tuple -- possible cause: successful or partial match";

	iterator_type error_position = std::get<1>(error);

	iterator_type from = std::abs(std::distance(std::get<0>(context), error_position)) < offset ? std::get<0>(context) : error_position - offset;
	iterator_type to = std::abs(std::distance(error_position, std::get<1>(context))) < offset ? std::get<1>(context) : error_position + offset;

	std::string before(from, error_position);
	std::string after(error_position + 1, to);
	std::string space(before.size(), ' ');

	for (size_t i = 0; i < before.size(); ++i)
		if (before[i] == '\t')
			space[i] = '\t';
	
	std::stringstream message;
	message	<< character_level_error_name << " failed while trying to match a(n) " << rule_level_error_name <<  ":\n" 
			<< before << std::endl << space << *error_position << std::endl
			<< space << " " << after << std::endl 
			<< space << "^";

	return message.str();
}

auto read_file_to_string(std::string_view const &filename) -> std::string
{
	std::fstream source(filename.data(), std::ios::in);
	if (!source)
		throw std::runtime_error
		(
			h119::util::string::format("Couldn't open file: %0", filename)
		);

	source >> std::noskipws;

	source.seekg (0, source.end);
	size_t length = source.tellg();
	source.seekg (0, source.beg);

	std::string the_contents;
	the_contents.reserve(length);
	std::copy
	(
			std::istream_iterator<char>(source),
			std::istream_iterator<char>(),
			std::inserter
			(
				the_contents,
				the_contents.end()
			)
	);

	return the_contents;
}

auto set_error_message
(
	iterator_type current_position,
	syntax_error &error,
	std::string const &description
) -> void

{
	if (current_position > std::get<1>(error) || std::get<0>(error) == "") {
		error = syntax_error(description, current_position, "");
	}
}

}
