#pragma once

#include <functional>
#include <memory>
#include <tuple>
#include <algorithm>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct string
{

	string
	(
		h119::syntx::character_type delimiter = '"',
		h119::syntx::character_type escape_character = '\\'
	):
		delimiter_{delimiter},
		escape_character_{escape_character}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[
			delimiter = this->delimiter_,
			escape_character = this->escape_character_
		]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			auto local_context = context;
			auto &current = std::get<0>(local_context);
			auto const &context_end = std::get<1>(local_context);

			if (*current == delimiter)
			{
				++current;

				while (current != context_end && *current != delimiter)
				{
					if (*current == escape_character) ++current;
					++current;
				}

				if (*current == delimiter)
				{
					++current;

					matching_range = h119::syntx::match_range{std::get<0>(context), current};
					context = local_context;

					return true;
				}
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A string (delimiter: \'%0\', escape character: \"%1\')",
					delimiter,
					escape_character
				)
			);

			return false;
		};
	}

	private:
		h119::syntx::character_type delimiter_;
		h119::syntx::character_type escape_character_;
};

}


