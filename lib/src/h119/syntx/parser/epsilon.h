#pragma once

#include <functional>
#include <memory>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct epsilon
{
	auto matcher() const -> parse_function 
	{
		return
		[]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &
		) -> bool
		{
			matching_range = match_range{std::get<0>(context), std::get<0>(context)};
			return true;
		};
	}
};

}


