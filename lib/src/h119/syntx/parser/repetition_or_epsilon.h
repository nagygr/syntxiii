#pragma once

#include <functional>
#include <memory>
#include <type_traits>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct repetition_or_epsilon
{
	repetition_or_epsilon(parse_function repeated_rule):
		repeated_rule_{repeated_rule}
	{}

	auto matcher() const -> parse_function
	{
		return 
		[repeated_rule = this->repeated_rule_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			h119::syntx::match_range local_context = context;
			h119::syntx::match_range match;

			std::get<0>(matching_range) = std::get<0>(context);
			std::get<1>(matching_range) = std::get<0>(context);

			while (repeated_rule(local_context, match, root, error))
			{
				std::get<1>(matching_range) = std::get<1>(match);
				context = local_context;

			}

			return true;
		};
	}

	private:
		parse_function repeated_rule_;
};

template
<
	typename repeated_rule_type,
	bool arg_ok =
		std::is_same
		<
			decltype(static_cast<repeated_rule_type*>(nullptr)->matcher()),
			parse_function
		>::value
>
auto operator *(repeated_rule_type repeated_rule) -> repetition_or_epsilon
{
	static_assert(arg_ok, "Repetition or epsilon argument is not a rule.");
	return repetition_or_epsilon{repeated_rule.matcher()};
}

}

