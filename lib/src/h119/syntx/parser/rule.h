#pragma once

#include <functional>
#include <memory>
#include <string_view>
#include <tuple>
#include <optional>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct rule
{
	rule():
		inner_{std::make_shared<parse_function>()},
		name_{std::nullopt}
	{}

	rule(std::string_view name):
		inner_{std::make_shared<parse_function>()},
		name_{name}
	{}

	template <typename assigned_rule>
	auto operator =(assigned_rule const &rhs) -> rule &
	{
		*inner_ = rhs.matcher();
		return *this;
	}

	auto matcher() const -> parse_function
	{
		return
		[inner = this->inner_, name = this->name_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (!*inner) {throw std::out_of_range("Uninitialized rule");}

			h119::syntx::match_range local_context = context, result;

			std::shared_ptr<h119::syntx::node> current_node = root;

			if (name)
				current_node = std::make_shared<h119::syntx::node>(name.value());

			if ((*inner)(local_context, result, current_node, error))
			{
				std::get<0>(matching_range) = std::get<0>(context);
				std::get<1>(matching_range) = std::get<1>(result);
				context = local_context;

				if (name)
				{
					log_5
					(
						"Adding node (%0) with match: %1 to %2",
						current_node->name,
						std::string{std::get<0>(matching_range), std::get<1>(matching_range)},
						root->name
					);
					current_node->matching_range(matching_range);
					root->children.push_back(current_node);
				}

				return true;

			}

			if (name && std::get<2>(error) == "") {
				std::get<2>(error) = name.value();
			}

			return false;
		};
	}

	private:
		std::shared_ptr<parse_function> inner_;
		std::optional<std::string> name_;
};

}
