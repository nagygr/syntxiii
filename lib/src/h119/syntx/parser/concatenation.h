#pragma once

#include <functional>
#include <memory>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct concatenation
{
	concatenation
	(
		parse_function first,
		parse_function second
	):
		first_{first},
		second_{second}
	{}

	auto matcher() const -> parse_function
	{
		return 
		[first = this->first_, second = this->second_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			h119::syntx::match_range local_context = context;
			h119::syntx::match_range lhs_match, rhs_match;

			if
			(
				first(local_context, lhs_match, root, error) &&
				second(local_context, rhs_match, root, error)
			)
			{
				std::get<0>(matching_range) = std::get<0>(lhs_match);
				std::get<1>(matching_range) = std::get<1>(rhs_match);
				context = local_context;

				log_5
				(
					"Concatenation matched (%0-%1)",
					*std::get<0>(matching_range),
					*(std::get<1>(matching_range) - 1)
				);

				return true;
			}

			return false;
		};
	}

	private:
		parse_function first_;
		parse_function second_;
};

template
<
	typename first,
	typename second,
	bool first_ok =
		std::is_same
		<
			decltype(static_cast<first*>(nullptr)->matcher()),
			parse_function
		>::value,
	bool second_ok =
		std::is_same
		<
			decltype(static_cast<second*>(nullptr)->matcher()),
			parse_function
		>::value
>
auto operator <<(first const &lhs, second const &rhs) -> concatenation
{
	static_assert(first_ok && second_ok, "Concatenation argument is not a rule.");
	return concatenation{lhs.matcher(), rhs.matcher()};
}

}
