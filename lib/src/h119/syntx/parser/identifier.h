#pragma once

#include <functional>
#include <memory>
#include <string_view>
#include <tuple>
#include <algorithm>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct identifier
{

	identifier(std::string_view extra_characters = "_"):
		extra_characters_{extra_characters}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[extra_characters = this->extra_characters_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			h119::syntx::match_range local_context = context;

			h119::syntx::character_type c = *std::get<0>(local_context);

			if
			(
				c == std::clamp(c, 'a', 'z') ||
				c == std::clamp(c, 'A', 'Z') ||
				extra_characters.find(c) != h119::syntx::string_type::npos
			)
			{
				auto &current = std::get<0>(local_context);

				++current;

				while
				(
					*current == std::clamp(*current, 'a', 'z') ||
					*current == std::clamp(*current, 'A', 'Z') ||
					*current == std::clamp(*current, '0', '9') ||
					extra_characters.find(*current) != h119::syntx::string_type::npos
				)
				{
					++current;
				}

				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(local_context)
				};

				context = local_context;

				return true;

			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"An identifier with extra characters:: \"%0\"",
					extra_characters
				)
			);
			return false;
		};
	}

	private:
		std::string extra_characters_;
};

}


