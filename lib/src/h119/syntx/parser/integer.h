#pragma once

#include <functional>
#include <memory>
#include <algorithm>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct integer
{
	enum class type
	{
		signed_int,
		unsigned_int
	};

	integer(type integer_type = type::signed_int):
		integer_type_{integer_type}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[integer_type = this->integer_type_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			bool is_negative = false;

			auto local_context = context;
			auto &current = std::get<0>(local_context);
			auto const &context_end = std::get<1>(local_context);

			if
			(
				integer_type == h119::syntx::parser::integer::type::signed_int &&
				*current == '-'
			)
			{
				is_negative = true;
				++current;
			}

			if (!is_negative && *current == '+') ++current;

			if (*current == '0')
			{
				++current;
				matching_range = h119::syntx::match_range{std::get<0>(context), current};
				context = local_context;

				return true;
			}

			if (*current == std::clamp(*current, '1', '9'))
			{
				for
				(
					++current;
					current != context_end &&
					*current == std::clamp(*current, '0', '9');
					++current
				);


				matching_range = h119::syntx::match_range{std::get<0>(context), current};
				context = local_context;

				log_5("Successfully matched a number: %0", std::string{std::get<0>(matching_range), std::get<1>(matching_range)});

				return true;
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A(n) %0 integer",
					integer_type == h119::syntx::parser::integer::type::signed_int ?
					"signed" :
					"unsigned"
				)
			);
			return false;
		};
	}

	private:
		type integer_type_;
};

}

