#pragma once

#include <functional>
#include <memory>
#include <tuple>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct not_in_range
{
	not_in_range(h119::syntx::character_type first, h119::syntx::character_type last):
		first_{first},
		last_{last}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[
			first = this->first_,
			last = this->last_
		]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			if (!(*std::get<0>(context) >= first && *std::get<0>(context) <= last))
			{
				matching_range = h119::syntx::match_range{std::get<0>(context), std::get<0>(context) + 1};
				++std::get<0>(context);

				return true;
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A not_in_range with characters between \'%0\' and \'%1\'",
					first,
					last
				)
			);
			return false;
		};
	}

	private:
		h119::syntx::character_type first_;
		h119::syntx::character_type last_;
};

}

