#pragma once

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct character
{
	character(std::string_view character_set):
		character_set_{character_set}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[character_set = this->character_set_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			for (char c: character_set)
			{
				if (c == *std::get<0>(context))
				{
					matching_range = h119::syntx::match_range{std::get<0>(context), std::get<0>(context) + 1};
					++std::get<0>(context);

					return true;
				}
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A character from the character set: \"%0\"",
					character_set
				)
			);
			return false;
		};
	}

	private:
		std::string character_set_;
};

}

