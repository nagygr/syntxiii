#pragma once

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>
#include <algorithm>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct keyword
{

	keyword(std::string_view the_word, std::string_view extra_characters = "_"):
		the_word_{the_word},
		extra_characters_{extra_characters}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[
			the_word = this->the_word_,
			extra_characters = this->extra_characters_
		]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			auto local_context = context;
			auto &current = std::get<0>(local_context);
			auto const &context_end = std::get<1>(local_context);

			std::string::const_iterator word_iterator = the_word.cbegin();
			std::string::const_iterator word_end = the_word.cend();
			while
			(
				word_iterator != word_end && 
				current != context_end &&
				*word_iterator == *current
			)
			{
				++word_iterator;
				++current;
			}

			if
			(
				word_iterator == word_end &&
				(
					current == context_end ||
					(
						*current != std::clamp(*current, 'a', 'z') &&
						*current != std::clamp(*current, 'A', 'Z') &&
						*current != std::clamp(*current, '0', '9') &&
						extra_characters.find(*current) == std::string::npos
					)
				)
			)
			{

				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(local_context)
				};

				context = local_context;

				return true;
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A keyword (\"%0\") with extra characters:: \"%1\"",
					the_word,
					extra_characters
				)
			);
			return false;
		};
	}

	private:
		std::string the_word_;
		std::string extra_characters_;
};

}

