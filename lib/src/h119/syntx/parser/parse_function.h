#pragma once

#include <functional>
#include <memory>
#include <tuple>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>

namespace h119::syntx::parser
{

using parse_function = std::function<auto(match_range &, match_range &, std::shared_ptr<h119::syntx::node> &, h119::syntx::syntax_error &)->bool>;

template <typename rule_type>
auto parse(rule_type const &rule_, match_range const &context, match_range &result, std::shared_ptr<h119::syntx::node> &node, h119::syntx::syntax_error &error) -> bool
{
	match_range local_context = context;
	bool matched = rule_.matcher()(local_context, result, node, error);

	return
		matched &&
		std::get<0>(context) == std::get<0>(result) &&
		std::get<1>(context) == std::get<1>(result);
}

}
