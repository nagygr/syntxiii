#pragma once

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>
#include <iterator>        // std::distance

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct substring
{

	substring(std::string_view the_word):
		the_word_{the_word}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[
			the_word = this->the_word_
		]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			auto local_context = context;
			auto &current = std::get<0>(local_context);
			auto const &context_end = std::get<1>(local_context);

			auto word_iterator = the_word.begin();
			auto const word_end = the_word.end();

			while
			(
				current != context_end &&
				word_iterator != word_end &&
				*current == *word_iterator
			)
			{
				++current;
				++word_iterator;
			}

			if
			(
				word_iterator == word_end &&
				static_cast<long int>(the_word.length()) == std::distance(std::get<0>(context), current)
			)
			{

				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					current
				};

				context = local_context;

				return true;
			}

			h119::syntx::set_error_message
			(
				std::get<0>(context),
				error,
				h119::util::string::format
				(
					"A substring (\"%0\")",
					the_word
				)
			);
			return false;

		};
	}

	private:
		std::string the_word_;
};

}

