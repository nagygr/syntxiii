#pragma once

#include <functional>
#include <memory>
#include <string_view>
#include <type_traits>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct option
{
	option(parse_function optional_rule):
		optional_rule_{optional_rule}
	{}

	auto matcher() const -> parse_function
	{
		return 
		[optional_rule = this->optional_rule_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			h119::syntx::match_range local_context = context;
			h119::syntx::match_range optional_match;

			if (optional_rule(local_context, optional_match, root, error))
			{
				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(local_context)
				};
				context = local_context;
			}
			else
			{
				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(context)
				};
			}

			return true;
		};
	}

	private:
		parse_function optional_rule_;
};

template
<
	typename optional_rule_type,
	bool =
		std::is_same
		<
			decltype(static_cast<optional_rule_type*>(nullptr)->matcher()),
			parse_function
		>::value
>
auto operator !(optional_rule_type optional_rule) -> option
{
	return option{optional_rule.matcher()};
}

}

