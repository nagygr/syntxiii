#pragma once

#include <algorithm>
#include <cstddef>
#include <functional>
#include <memory>
#include <string_view>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>

namespace h119::syntx::parser
{

struct real
{
	enum class decimal_mark {
		point,
		comma
	};

	static constexpr char const *decimal_mark_character = ".,";

	real(decimal_mark decimal_mark_ = decimal_mark::point):
		decimal_mark_{decimal_mark_}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[decimal_mark_ = this->decimal_mark_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &,
			h119::syntx::syntax_error &error
		) -> bool
		{
			if (std::get<0>(context) == std::get<1>(context)) return false;

			auto local_context = context;
			auto &current = std::get<0>(local_context);

			char decimal = decimal_mark_character[static_cast<size_t>(decimal_mark_)]; 
			std::string_view exponent = "eE";

			bool has_sign = false, has_int_part = false, has_decimal = false;
			bool has_frac_part = false;
			bool has_exponent_sign = false, has_exponent_letter = false;
			bool has_exponent = false;
			bool single_digit_int_part = false;

			if (*current == '-' || *current == '+')
			{
				++current;
				has_sign = true;
			}

			if (*current == '0')
			{
				++current;
				has_int_part = true;
				single_digit_int_part = true;
			}
			else
			{
				if (*current == std::clamp(*current, '1', '9'))
				{
					++current;
					has_int_part = true;

					while (*current == std::clamp(*current, '0', '9'))
						++current;
				}
			}

			if (*current == decimal)
			{
				++current;
				has_decimal = true;
			}

			if (*current == std::clamp(*current, '0', '9'))
			{
				++current;
				has_frac_part = true;

				while (*current == std::clamp(*current, '0', '9'))
					++current;
			}

			if (exponent.find(*current) != h119::syntx::string_type::npos)
			{
				++current;
				has_exponent_letter = true;

				if (*current == '-' || *current == '+')
				{
					++current;
					has_exponent_sign = true;
				}

				if (*current == std::clamp(*current, '0', '9'))
				{
					++current;
					has_exponent = true;

					while (*current == std::clamp(*current, '0', '9'))
						++current;
				}
			}

			/*
			 * The 
			 *  (has_exponent_letter && has_exponent_sign && !has_exponent) ||
			 *  (has_exponent_letter && !has_exponent_sign && !has_exponent)
			 * expression could be simplified by elminating has_exponent_sign but
			 * it is used like this intentionally to draw attention to both 
			 * error cases.
			 */
			if
			(
				(current == std::get<0>(context)) ||
				(has_sign && !has_int_part && !has_decimal && !has_exponent_letter) ||
				(single_digit_int_part && !has_decimal && has_frac_part) ||
				(has_sign && !has_int_part && !has_frac_part && has_exponent_letter) ||
				(has_exponent_letter && has_exponent_sign && !has_exponent) ||
				(has_exponent_letter && !has_exponent_sign && !has_exponent)
			) {

				h119::syntx::set_error_message
				(
					std::get<0>(context),
					error,
					h119::util::string::format
					(
						"A real (decimal mark: \'%0\')",
						decimal_mark_character
						[
							static_cast<std::size_t>(h119::syntx::parser::real::decimal_mark::point)
						]
					)
				);
				return false;
			}

			matching_range = h119::syntx::match_range{std::get<0>(context), current};
			context = local_context;
			return true;
		};
	}

	private:
		decimal_mark decimal_mark_;
};

}


