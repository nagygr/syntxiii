#pragma once

#include <functional>
#include <memory>
#include <tuple>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct separated_list
{
	separated_list(parse_function repeated_rule, parse_function separator):
		repeated_rule_{repeated_rule},
		separator_{separator}
	{}

	auto matcher() const -> parse_function
	{
		return 
		[
			repeated_rule = this->repeated_rule_,
			separator = this->separator_
		]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			h119::syntx::match_range local_context = context;
			h119::syntx::match_range match;

			if (repeated_rule(local_context, match, root, error))
			{
				
				bool full_match = false;
				
				do
				{
					auto inner_local_context = local_context;

					full_match = false;

					if (separator(inner_local_context, match, root, error))
					{
						if (repeated_rule(inner_local_context, match, root, error))
						{
							local_context = inner_local_context;
							full_match = true;
						}
						else return false;
					}
				}
				while (full_match);

				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(local_context)
				};
				context = local_context;

				return true;
			}

			return false;
		};
	}

	private:
		parse_function repeated_rule_;
		parse_function separator_;
};

template
<
	typename repeated_rule_type,
	typename separator_type,
	bool repeated_ok =
		std::is_same
		<
			decltype(static_cast<repeated_rule_type*>(nullptr)->matcher()),
			parse_function
		>::value,
	bool separator_ok =
		std::is_same
		<
			decltype(static_cast<separator_type*>(nullptr)->matcher()),
			parse_function
		>::value
>
auto operator %(repeated_rule_type repeated_rule, separator_type separator) -> separated_list
{
	static_assert(repeated_ok && separator_ok, "Separated list argument is not a rule.");
	return separated_list{repeated_rule.matcher(), separator.matcher()};
}

}

