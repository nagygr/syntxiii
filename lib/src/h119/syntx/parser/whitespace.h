#pragma once

#include <functional>
#include <memory>
#include <tuple>
#include <cctype>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct whitespace
{
	whitespace(parse_function inner_rule):
		inner_rule_{inner_rule}
		{}
	
	auto matcher() const -> parse_function 
	{
		return
		[inner_rule = this->inner_rule_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			auto local_context = context;
			auto &current = std::get<0>(local_context);
			auto const &context_end = std::get<1>(local_context);

			/*
			 * Deliberately not checking end of context as -epsilon
			 * would fail for an iterator at the end but it shouldn't.
			 * The inner rule should check for the end anyway.
			 */

			h119::syntx::match_range local_result;

			while
			(
				current != context_end &&
				std::isspace(*current)
			)
			{
				++current;
			}

			if (inner_rule(local_context, local_result, root, error))
			{
				matching_range = h119::syntx::match_range
				{
					std::get<0>(context),
					std::get<0>(local_context)
				};
				context = local_context;

				return true;
			}

			return false;
		};
	}

	private:
		parse_function inner_rule_;
};

template
<
	typename inner_rule_type,
	bool arg_ok =
		std::is_same
		<
			decltype(static_cast<inner_rule_type*>(nullptr)->matcher()),
			parse_function
		>::value
>
auto operator -(inner_rule_type inner_rule) -> whitespace
{
	static_assert(arg_ok, "Whitespace argument is not a rule.");
	return whitespace{inner_rule.matcher()};
}

}

