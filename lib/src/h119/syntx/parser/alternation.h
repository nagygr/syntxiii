#pragma once

#include <functional>
#include <memory>

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>
#include <h119/syntx/parser/parse_function.h>
#include <h119/util/string/format.h>
#include <h119/util/string/logger.h>

namespace h119::syntx::parser
{

struct alternation
{
	alternation
	(
		parse_function first,
		parse_function second
	):
		first_{first},
		second_{second}
	{}

	auto matcher() const -> parse_function
	{
		return 
		[first = this->first_, second = this->second_]
		(
			h119::syntx::match_range &context,
			h119::syntx::match_range &matching_range,
			std::shared_ptr<h119::syntx::node> &root,
			h119::syntx::syntax_error &error
		) -> bool
		{
			h119::syntx::match_range local_context = context;
			h119::syntx::match_range lhs_match, rhs_match;

			if
			(
				first(local_context, lhs_match, root, error)
			)
			{
				matching_range = lhs_match;
				context = local_context;

				log_5
				(
					"Alternation matched (1st argument): %0",
					std::string
					{
						std::get<0>(matching_range),
						std::get<1>(matching_range)
					}
				);

				return true;
			}

			if
			(
				second(local_context, rhs_match, root, error)
			)
			{
				matching_range = rhs_match;
				context = local_context;

				log_5
				(
					"Alternation matched (2nd argument): %0",
					std::string
					{
						std::get<0>(matching_range),
						std::get<1>(matching_range)
					}
				);

				return true;
			}

			return false;
		};
	}

	private:
		parse_function first_;
		parse_function second_;
};

template <typename first, typename second>
auto operator |(first const &lhs, second const &rhs) -> alternation
{
	return alternation{lhs.matcher(), rhs.matcher()};
}

}

