#pragma once

#include <string>
#include <string_view>
#include <tuple>
#include <functional>

namespace h119::syntx
{

using string_type = std::string_view;
using character_type = string_type::value_type;
using iterator_type = string_type::const_iterator;

using match_range = std::tuple<iterator_type, iterator_type>;
using syntax_error = std::tuple<std::string, iterator_type, std::string>;

auto error_message(match_range const &context, syntax_error const &error) -> std::string;
auto read_file_to_string(std::string_view const &filename) -> std::string;
auto set_error_message
(
	iterator_type current_position,
	syntax_error &error,
	std::string const &description
) -> void;

}
