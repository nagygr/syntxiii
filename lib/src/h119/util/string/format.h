#pragma once

#include <cstddef>         // size_t
#include <cctype>          // std::isdigit
#include <string>          // std::string
#include <sstream>         // std::stringstream

namespace h119::util::string
{

template < typename T >
auto to_string(T const &arg) -> std::string 
{
	std::ostringstream os;
	os << std::boolalpha << arg;
	return os.str();
}

inline auto to_string(char const *arg) -> std::string
{
	return std::string(arg);
}

template <class T, std::size_t N>
constexpr auto array_size(const T (&)[N]) noexcept
{
	return N;
}

template <typename ...ARGS>
auto format(char const *format_str, ARGS const & ...args) -> std::string
{
	/* The empty string as the first element of the array is needed, because
	 * zero element arrays are not permitted in C++, and in case of no extra
	 * arguments after the format string, "strings" would be empty. (Fortunately,
	 * { "", } is a valid array initializer expression).
	 *
	 * This is also the reason why index is incremented after the for loop below.
	 */
	std::string strings [] = { "", to_string(args)... }; 
	std::size_t strings_size = array_size(strings);
	std::stringstream stream;

	for (int i = 0; format_str[i] != '\0';)
	{
		if (format_str[i] == '%')
		{
			if (format_str[i + 1] == '%')
			{
				stream << "%";
				i += 2;
			}
			else
			{
				std::size_t index = 0;
				for (++i; std::isdigit(format_str[i]); ++i) index = index * 10 + (format_str[i] - '0');
				++index;

				if (index < strings_size)
				{
					stream << strings[index];
				}
			}

		}
		else
		{
			stream << format_str[i];
			++i;
		}
	}
	return stream.str();
}

}

