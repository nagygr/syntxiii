#pragma once

#include <iostream>        // std::cout
#include <sstream>         // std::stringstream
#include <cctype>          // std::isdigit
#include <cstddef>         // size_t
#include <chrono>          // std::chrono::*
#include <ctime>           // std::time_t
#include <iomanip>         // std::put_time

#include <h119/util/string/format.h>

namespace h119::util::string {

template <typename ...ARGS>
auto print_log
(
	std::ostream &stream,
	char const *file_name,
	int line_number,
	char const *function_name,
	char log_level,
	char const *format,
	ARGS const & ...args
) -> void
{

	/* The empty string as the first element of the array is needed, because
	 * zero element arrays are not permitted in C++, and in case of no extra
	 * arguments after the format string, "strings" would be empty. (Fortunately,
	 * { "", } is a valid array initializer expression).
	 *
	 * This is also the reason why index is incremented after the for loop below.
	 */
	std::string strings [] = { "", to_string(args)... }; 
	std::size_t strings_size = h119::util::string::array_size(strings);

	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_c = std::chrono::system_clock::to_time_t(now);

	stream << "[" << std::put_time(std::localtime(&now_c), "%c %Z") << "] [" << log_level << "] ";

	for (int i = 0; format[i] != '\0';) {
		if (format[i] == '%') {
			if (format[i + 1] == '%') {
				stream << "%";
				i += 2;
			}
			else {
				std::size_t index = 0;
				for (++i; std::isdigit(format[i]); ++i) index = index * 10 + (format[i] - '0');
				++index;

				if (index < strings_size) {
					stream << strings[index];
				}
			}

		} else {
			stream << format[i];
			++i;
		}
	}

	stream << " (" << file_name << ":" << line_number << " (" << function_name << "))" << std::endl;
}

/*
 * Compile with -D LOGLEVEL=<log level> to enable logging
 * Level 1 -- E: error
 * Level 2 -- W: warning
 * Level 3 -- I: information (preferably restricted to state changes)
 * Level 4 -- D: debug (details of the operation)
 * Level 5 -- V: verbose (fine details of the operation)
 */

#define log_1(...) 
#define log_2(...) 
#define log_3(...) 
#define log_4(...) 
#define log_5(...) 

#define OUTPUT_STREAM std::cerr

#if LOGLEVEL >= 1
#	undef log_1
#	define log_1(...) h119::util::string::print_log(OUTPUT_STREAM, __FILE__, __LINE__, __FUNCTION__, 'E', __VA_ARGS__); 
#endif

#if LOGLEVEL >= 2
#	undef log_2
#	define log_2(...) h119::util::string::print_log(OUTPUT_STREAM, __FILE__, __LINE__, __FUNCTION__, 'W', __VA_ARGS__); 
#endif

#if LOGLEVEL >= 3
#	undef log_3
#	define log_3(...) h119::util::string::print_log(OUTPUT_STREAM, __FILE__, __LINE__, __FUNCTION__, 'I', __VA_ARGS__); 
#endif


#if LOGLEVEL >= 4
#	undef log_4
#	define log_4(...) h119::util::string::print_log(OUTPUT_STREAM, __FILE__, __LINE__, __FUNCTION__, 'D', __VA_ARGS__); 
#endif


#if LOGLEVEL >= 5
#	undef log_5
#	define log_5(...) h119::util::string::print_log(OUTPUT_STREAM, __FILE__, __LINE__, __FUNCTION__, 'V', __VA_ARGS__); 
#endif

}

