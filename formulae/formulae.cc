#include <iostream>
#include <unordered_map>

#include <h119/formulae/expression.h>

auto main() -> int
{
	enum class result {ok, parsing_failure, evaluation_failure};

	try
	{
		h119::formulae::expression e;

		e.parse("2.34 * y / (-.4 * sin( x = 5e-1 ))");

		/*
		 * If the AST is of interest, it can be printed using print_tree():
		 * h119::syntx::print_tree(std::cout, e.ast(), h119::syntx::tree_verbosity::concise);
		 */

		std::unordered_map<std::string, double> variables =
		{
			{"y", 1.0}
		};

		auto expression = e.build_expression();

		std::cout << "Result: " << expression->evaluate(variables) << std::endl;
	}
	catch (h119::formulae::expression::parse_error const &pe)
	{
		std::cout << "Parsing failure" << std::endl;
		std::cout << pe.what() << std::endl;
		return static_cast<int>(result::parsing_failure);
	}
	catch (h119::formulae::expression::evaluation_error const &ee)
	{
		std::cout << "Evaluation failure" << std::endl;
		std::cout << ee.what() << std::endl;
		return static_cast<int>(result::evaluation_failure);
	}
	
	return static_cast<int>(result::ok);
}
