#include <iostream>

#include <h119/formulae/expression.h>

#include <h119/util/string/format.h>
#include <h119/formulae/elements/expression.h>
#include <h119/formulae/elements/addition.h>
#include <h119/formulae/elements/subtraction.h>
#include <h119/formulae/elements/multiplication.h>
#include <h119/formulae/elements/division.h>
#include <h119/formulae/elements/constant.h>
#include <h119/formulae/elements/function.h>
#include <h119/formulae/elements/variable.h>
#include <h119/formulae/elements/assignment.h>

namespace h119::formulae
{

expression::expression():
	expression_{"expression"},
	sumsub_{"sumsub"},
	muldiv_{"muldiv"},
	value_{"value"},
	funcall_{"funcall"},
	assignment_{"assignment"},
	paren_{"paren"},
	variable_{"variable"},
	number_{"number"},
	funname_{"funname"},
	plus_{"plus"},
	minus_{"minus"},
	slash_{"slash"},
	star_{"star"},
	root_{std::make_shared<h119::syntx::node>("root")}
{
	{
		using namespace	h119::syntx::parser;

		expression_ = sumsub_;
		sumsub_ = muldiv_ << !((-plus_ | -minus_) << sumsub_);
		muldiv_ = -value_ << !((-star_ | -slash_) << muldiv_);
		value_ = -number_ | -funcall_ | assignment_ | -variable_ | paren_;
		funcall_ = -funname_ << -character{"("} << expression_ << -character{")"};
		assignment_ = -variable_ << -character{"="} << expression_;
		paren_ = -character{"("} << expression_ << -character{")"};
		variable_ = identifier{};
		number_ = real{};
		funname_ = identifier{};
		plus_ = character{"+"};
		minus_ = character{"-"};
		slash_ = character{"/"};
		star_ = character{"*"};
	}
}

auto expression::parse(std::string_view text) -> bool
{
	h119::syntx::match_range context{text.begin(), text.end()}, result;
	h119::syntx::syntax_error error;

	if (!h119::syntx::parser::parse(expression_, context, result, root_, error))
	{
		throw parse_error{h119::syntx::error_message(context, error)};
	}

	return true;
}

auto expression::build_helper(std::shared_ptr<h119::syntx::node> &node) ->
	std::shared_ptr<h119::formulae::elements::expression>
{
	if 
	(
		node->name == "root" ||
		node->name == "paren" ||
		node->name == "value"
	)
		return build_helper(node->children[0]);

	if (node->name == "sumsub")
	{
		if (node->children.size() == 1) return build_helper(node->children[0]);
		else if (node->children.size() == 3)
		{
			char operation = *std::get<0>(node->children[1]->matching_range());

			switch (operation)
			{
				case '+':
					return std::make_shared<h119::formulae::elements::addition>
					(
						build_helper(node->children[0]),
						build_helper(node->children[2])
					);
				case '-':
					return std::make_shared<h119::formulae::elements::subtraction>
					(
						build_helper(node->children[0]),
						build_helper(node->children[2])
					);
			}
		}
	}

	if (node->name == "muldiv")
	{
		if (node->children.size() == 1) return build_helper(node->children[0]);
		else if (node->children.size() == 3)
		{
			char operation = *std::get<0>(node->children[1]->matching_range());

			switch (operation)
			{
				case '*':
					return std::make_shared<h119::formulae::elements::multiplication>
					(
						build_helper(node->children[0]),
						build_helper(node->children[2])
					);
				case '/':
					return std::make_shared<h119::formulae::elements::division>
					(
						build_helper(node->children[0]),
						build_helper(node->children[2])
					);
			}
		}
	}

	if (node->name == "number")
	{
		auto range = node->matching_range();
		return std::make_shared<h119::formulae::elements::constant>
		(
			std::string
			{
				std::get<0>(range),
				std::get<1>(range)
			}
		);
	}

	if (node->name == "variable")
	{
		auto range = node->matching_range();
		return std::make_shared<h119::formulae::elements::variable>
		(
			std::string
			{
				std::get<0>(range),
				std::get<1>(range)
			}
		);
	}

	if (node->name == "assignment")
	{
		auto variable_name_range = node->children[0]->matching_range();
		return  std::make_shared<h119::formulae::elements::assignment>
		(
			std::string
			{
				std::get<0>(variable_name_range),
				std::get<1>(variable_name_range)
			},
			build_helper(node->children[1])
		);
	}

	if (node->name == "funcall")
	{
		auto function_name_range = node->children[0]->matching_range();
		return  std::make_shared<h119::formulae::elements::function>
		(
			std::string
			{
				std::get<0>(function_name_range),
				std::get<1>(function_name_range)
			},
			build_helper(node->children[1])
		);

	}

	throw evaluation_error
	{
		h119::util::string::format
		(
			"Unexpected AST element: %0",
			node->name
		)
	};
}

}
