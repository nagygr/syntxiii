#pragma once

#include <string>
#include <string_view>
#include <stdexcept>
#include <unordered_map>
#include <functional>
#include <cmath>

#include <h119/formulae/elements/expression.h>

#include <h119/util/string/format.h>

namespace h119::formulae::elements
{

struct function: expression
{
	struct undefined_function: std::runtime_error
	{
		undefined_function(std::string_view function_name):
			std::runtime_error
			{
				h119::util::string::format
				(
					"The function named %0 is undefined",
					function_name
				)
			}
			
		{}
	};

	function(std::string const &function_name, std::shared_ptr<expression> argument):
		function_name_{function_name},
		argument_{argument}
	{
		functions_["sin"] = [](double d) -> double {return std::sin(d);};
		functions_["cos"] = [](double d) -> double {return std::cos(d);};
	}

	virtual auto evaluate(std::unordered_map<std::string, double> &variables) -> double override
	{
		if (functions_.find(function_name_) == functions_.end())
			throw undefined_function(function_name_);

		return functions_[function_name_](argument_->evaluate(variables));
	}
	
	private:
		std::string function_name_;
		std::shared_ptr<expression> argument_;
		std::unordered_map<std::string_view, std::function<auto(double)->double>> functions_;
};

}
