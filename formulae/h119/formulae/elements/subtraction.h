#pragma once

#include <string>
#include <memory>
#include <unordered_map>

#include <h119/formulae/elements/expression.h>

namespace h119::formulae::elements
{

struct subtraction: expression
{
	subtraction(std::shared_ptr<expression> lhs, std::shared_ptr<expression> rhs):
		lhs_{lhs}, rhs_{rhs}
	{}

	virtual auto evaluate(std::unordered_map<std::string, double> &variables) -> double override
	{
		return lhs_->evaluate(variables) - rhs_->evaluate(variables);
	}
	
	private:
		std::shared_ptr<expression> lhs_;
		std::shared_ptr<expression> rhs_;
};

}


