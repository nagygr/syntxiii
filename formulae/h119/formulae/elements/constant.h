#pragma once

#include <string>
#include <string_view>

#include <h119/formulae/elements/expression.h>

namespace h119::formulae::elements
{

struct constant: expression
{
	constant(std::string_view value_string):
		value_{std::stod(value_string.data())}
	{}

	virtual auto evaluate(std::unordered_map<std::string, double> &) -> double override
	{
		return value_;
	}
	
	private:
		double value_;
};

}
