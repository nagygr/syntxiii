#pragma once

#include <string>
#include <memory>
#include <unordered_map>

#include <h119/formulae/elements/expression.h>

namespace h119::formulae::elements
{

struct assignment: expression
{
	assignment(std::string const &variable_name, std::shared_ptr<expression> rhs):
		variable_name_{variable_name}, rhs_{rhs}
	{}

	virtual auto evaluate(std::unordered_map<std::string, double> &variables) -> double override
	{
		double value = rhs_->evaluate(variables);
		
		if (variables.find(variable_name_) == variables.end())
			variables.emplace(std::pair{variable_name_, value});
		else
			variables[variable_name_] = value;

		return value;
	}
	
	private:
		std::string variable_name_;
		std::shared_ptr<expression> rhs_;
};

}



