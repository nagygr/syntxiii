#pragma once

#include <string>
#include <stdexcept>

#include <h119/formulae/elements/expression.h>

#include <h119/util/string/format.h>

namespace h119::formulae::elements
{

struct variable: expression
{
	struct undefined_variable: std::runtime_error
	{
		undefined_variable(std::string_view variable_name):
			std::runtime_error
			{
				h119::util::string::format
				(
					"The variable named %0 is undefined",
					variable_name
				)
			}
			
		{}
	};

	variable(std::string const &variable_name):
		variable_name_{variable_name}
	{}

	virtual auto evaluate(std::unordered_map<std::string, double> &variables) -> double override
	{
		if (variables.find(variable_name_) == variables.end())
			throw undefined_variable(variable_name_);

		return variables[variable_name_];
	}
	
	private:
		std::string variable_name_;
};

}
