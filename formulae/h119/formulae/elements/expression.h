#pragma once

#include <string>
#include <unordered_map>

namespace h119::formulae::elements
{

struct expression
{
	virtual auto evaluate(std::unordered_map<std::string, double> &variables) -> double = 0;
};

}
