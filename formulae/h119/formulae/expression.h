#pragma once

#include <memory>
#include <stdexcept>

#include <h119/syntx/syntx.h>
#include <h119/formulae/elements/expression.h>

namespace h119::formulae
{

struct expression
{
	struct parse_error: std::runtime_error
	{
		parse_error(std::string_view text): std::runtime_error{text.data()} {}
	};

	struct evaluation_error: std::runtime_error
	{
		evaluation_error(std::string_view text): std::runtime_error{text.data()} {}
	};

	expression();

	auto parse(std::string_view text) -> bool;

	auto ast() -> std::shared_ptr<h119::syntx::node>
	{
		return root_;
	}

	auto build_expression() -> std::shared_ptr<h119::formulae::elements::expression>
	{
		return build_helper(root_);
	}

	private:
		h119::syntx::parser::rule expression_;
		h119::syntx::parser::rule sumsub_;
		h119::syntx::parser::rule muldiv_;
		h119::syntx::parser::rule value_;
		h119::syntx::parser::rule funcall_;
		h119::syntx::parser::rule assignment_;
		h119::syntx::parser::rule paren_;
		h119::syntx::parser::rule variable_;
		h119::syntx::parser::rule number_;
		h119::syntx::parser::rule funname_;
		h119::syntx::parser::rule plus_;
		h119::syntx::parser::rule minus_;
		h119::syntx::parser::rule slash_;
		h119::syntx::parser::rule star_;

		std::shared_ptr<h119::syntx::node> root_;


		auto build_helper(std::shared_ptr<h119::syntx::node> &node) -> 
			std::shared_ptr<h119::formulae::elements::expression>;
};

}
